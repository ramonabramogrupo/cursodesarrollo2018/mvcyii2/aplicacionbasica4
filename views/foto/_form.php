<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;
use app\models\Articulo;

/* @var $this yii\web\View */
/* @var $model app\models\Foto */
/* @var $form yii\widgets\ActiveForm */

$articulos=Articulo::find()->all();
$listado=ArrayHelper::map($articulos, "id", "titulo");

?>

<div class="foto-form">

    <?php $form = ActiveForm::begin(); ?>
    
    <?= $form->field($model, 'articulo')->dropDownList($listado); ?>

    <?= $form->field($model, 'nombre')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'alt')->textInput(['maxlength' => true]) ?>

    <div class="form-group">
        <?= Html::submitButton('Save', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
