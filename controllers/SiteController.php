<?php

namespace app\controllers;

use Yii;
use yii\web\Controller;
use yii\web\Response;
use app\models\Articulo;



class SiteController extends Controller
{
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
        ];
    }

    public function actionIndex()
    {
        $datos= Articulo::find()->all();
        
        //$datos1= Articulo::find()->with('fotos')->all();
        //var_dump($datos1);
        //exit;
        return $this->render('index',[
            "articulos"=>$datos,
        ]);
    }
    
    public function actionBack(){
        return $this->render("back");
    }

}
